import React from 'react';
class BoutonNbClick extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        isToggleOn: true,
        nbClick: 0
    };
    this.handleClick=this.handleClick.bind(this);  // Cette liaison est nécéssaire afin de permettre    
    // l'utilisation de `this` dans la fonction de rappel.
    }
  
    handleClick() {
        this.setState({
            isToggleOn: !this.state.isToggleOn,
            nbClick: this.state.nbClick + 1
        })
        }
    render() {
      return (
        <button onClick={this.handleClick}>
            {this.state.isToggleOn ? 'ON' : 'OFF'}
            <br/>
            {this.state.nbClick}
        </button>
      );
    }
  }

export default BoutonNbClick;