import logo from './logo.svg';
import './App.css';
import React from 'react';
import HelloMessage from './components/HelloMessage'
import Timer from './components/Timer'
import BoutonNbClick from './components/BoutonNbClick'


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <HelloMessage name="Mariam" surname="JABER" color="white"/>
        <Timer delay= "1000"/>
        <Timer delay= "2000"/>
        <Timer/>
        <BoutonNbClick/>
        
      </header>
    </div>
  );
}

export default App;
