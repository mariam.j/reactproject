import React from 'react';

class Timer extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        seconds: 0,
        //paire: -1,
        //choixInput: -1
       };
    }
  
    tick() {
      this.setState(state => ({
        seconds: this.state.seconds + 1,
        //paire: this.state.seconds %2 === 0 ? true : false ,
      }));
    }
  
    componentDidMount() {
      const delay = this.props.delay
      const realDelay = delay ? delay : 1000
      this.interval = setInterval(() => this.tick(), realDelay);
    }
  
    /*componentWillUnmount() {
      clearInterval(this.interval);
    }*/
  
    render() {
      const delay = this.props.delay
      return (
        <div>
          Secondes : {this.state.seconds} Delay: { delay }
        </div>
      );
    }
  }
  export default Timer;