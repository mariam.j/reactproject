import React from 'react';


class HelloMessage extends React.Component {
    render() {
      const { surname, name } = this.props
      const len = surname? surname.length : "(error props suname undefined)"
      const divStyle = {
        color: this.props.color,
      };


      return (
        <div style={divStyle} >
          Hello {name} {surname}
          <br/> length of surname {len} letters
        </div>
      );
    }
  } 
export default HelloMessage;